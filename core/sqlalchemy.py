from pytz import timezone
import datetime
from flask_sqlalchemy import SQLAlchemy
import logging

db = SQLAlchemy()

class BaseModel(db.Model):
    __abstract__ = True

    SQL_STATEMENT = ''
    EXCLUDED_ATTRIBUTES = ['meta_created_on', 'meta_updated_on', 'meta_created_by', 'meta_updated_by', 'id']

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    meta_created_on = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    meta_updated_on = db.Column(db.DateTime, onupdate=datetime.datetime.utcnow())

    # TODO: Pull user details
    meta_created_by = db.Column(db.String(255), default='anonymous')
    meta_updated_by = db.Column(db.String(255), default='anonymous')

    @classmethod
    def create_table(cls):
        for table in db.metadata.tables:
            if table == cls.__table__.name:
                cls.__table__.create(db.engine)

    @classmethod
    def get(cls, key):
        return db.session.query(cls).get(key)

    @classmethod
    def get_by(cls, **kw):
        return db.session.query(cls).filter_by(**kw).first()

    @classmethod
    def create(cls, **kw):
        o = cls(**kw)
        db.session.add(o)
        db.session.commit()
        return o

    @classmethod
    def create(cls, obj):
        db.session.add(obj)
        db.session.commit()
        return obj

    @classmethod
    def all(cls):
        items = []
        for i in db.session.query(cls).all():
            items.append(i.to_dict())
        return items

    @classmethod
    def query(cls, page=1, size=100, sort=None, order='desc', **kw):
        query = db.session.query(cls).filter_by(**kw)

        if sort:
            if order == 'desc':
                query = query.order_by(cls.__getattribute__(cls, sort).desc())
            else:
                query = query.order_by(cls.__getattribute__(cls, sort))
        else: # sort in descending order according to id
            query = query.order_by(cls.__getattribute__(cls, 'id').desc())

        if size:
            query = query.limit(int(size))

        if page:
            offset = (int(page) - 1) * int(size)
            query = query.offset(offset)

        items = []
        for i in query.all():
            items.append(i.to_dict())
        
        return items

    @classmethod
    def count(cls, **kw):
        return db.session.query(cls).filter_by(**kw).count()

    def save(self):
        db.session.add(self)
        db.session.flush()
        exec("self.{}_id = '{}-{}'".format(self.__singular_name__, self.__singular_name__.upper(), self.id))
        db.session.commit()
        return self

    def update(self):
        db.session.commit()
        return self

    def delete(self):
        db.session.delete(self)

    def refresh(self):
        db.session.refresh(self)

    def to_dict(self):
        return dict((k, (timezone('UTC').localize(getattr(self, k)).isoformat())
            if isinstance(getattr(self, k), datetime.datetime) else getattr(self, k))
            for k in self.__table__.c.keys() if k not in self.EXCLUDED_ATTRIBUTES)

    def __repr__(self):
        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in self.__table__.c.keys())
        return "%s(%s)" % (self.__class__.__name__, values)
