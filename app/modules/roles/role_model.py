import logging
from app.exceptions import EntityNotFoundException
from core.sqlalchemy import BaseModel, db

class RoleModel(BaseModel):
    __ddl__ = """
        CREATE TABLE `roles` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `is_deleted` boolean DEFAULT 0,
          `meta_created_by` varchar(255) DEFAULT NULL,
          `meta_created_on` datetime DEFAULT NULL,
          `meta_updated_by` varchar(255) DEFAULT NULL,
          `meta_updated_on` datetime DEFAULT NULL,
          `role_description` varchar(255) DEFAULT NULL,
          `role_id` int(11) NOT NULL,
          `role_name` varchar(128) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1
    """
    EXCLUDED_ATTRIBUTES = BaseModel.EXCLUDED_ATTRIBUTES + ['is_deleted']

    __tablename__ = 'roles'
    __singular_name__ = 'role'

    is_deleted = db.Column(db.Boolean, default=False)
    role_description = db.Column(db.String(255), nullable=True)
    role_id = db.Column(db.String(255), unique=True)
    role_name = db.Column(db.String(255), unique=True, nullable=False)

    def __init__(self, role_id, role_name, role_description):
        self.role_id = role_id
        self.role_name = role_name
        self.role_description = role_description

    @classmethod
    def get_all(cls, request, **properties):
        page = request.get('page', 1)
        size = request.get('size', 100)
        sort = request.get('sort')
        order = request.get('order')

        return cls.query(page, size, sort, order, **properties)

    @classmethod
    def get_by_id(cls, id):
        role = cls.get_by(is_deleted=False, role_id=id)
        
        if role is None:
            raise EntityNotFoundException("No role with '{}' ID exists.".format(id))

        return role.to_dict()

    @classmethod
    def create_role(cls, **properties):
        role = cls(**properties).save()
        return role.to_dict()

    @classmethod
    def update_role(cls, id, **updates):
        role = cls.get_by(is_deleted=False, role_id=id)

        if role is None:
            raise EntityNotFoundException("No role with '{}' ID exists.".format(id))

        role.role_name = updates['role_name']
        role.role_description = updates['role_description']
        
        return role.update().to_dict()

    @classmethod
    def delete_role(cls, role_id):
        role = cls.get_by(is_deleted=False, role_id=role_id)

        if role is None:
            raise EntityNotFoundException("No role with '{}' ID exists.".format(role_id))

        role.is_deleted = True

        return role.update().to_dict()

    @classmethod
    def get_role_list_by_ids(cls, values):
        roles = db.session.query(cls).filter(cls.role_id.in_(values)).all()

        if len(roles) == 0:
            raise EntityNotFoundException("No roles found with the following ids {}.".format(values))

        roleList = []
        for role in roles:
            roleList.append({"id": role.role_id, "name": role.role_name})

        return roleList
