import logging
import app.constants as constants
from flask import Blueprint, request
from core.utils import create_json_response, handle_error
from app.modules.roles.role_model import RoleModel
from app.exceptions import InvalidFormatException

roles = Blueprint('roles', __name__, url_prefix=constants.ROLE_URL_PREFIX + constants.ROLE_API)


@roles.before_request
def before_post():
	try:
		if request.method != 'POST':
			return None

		req_json = request.get_json()

		if req_json is None:
			return None

		include = RoleModel.__table__.columns
		exclude = RoleModel.EXCLUDED_ATTRIBUTES

		if not all(key not in exclude and key in include for key in req_json):
			raise InvalidFormatException(
				"Some fields cannot be edited.")

	except Exception as error:
		handle_error(error)


@roles.before_request
def before_put():
	try:
		if request.method != 'PUT':
			return None

		req_json = request.get_json()

		if req_json is None:
			return None

		include = RoleModel.__table__.columns
		exclude = RoleModel.EXCLUDED_ATTRIBUTES

		if not all(key not in exclude and key in include for key in req_json):
			raise InvalidFormatException(
				"Some fields cannot be edited.")

	except Exception as error:
		handle_error(error)


@roles.before_request
def before_delete():
	if request.method != 'DELETE':
		return None


@roles.route("/init_table", methods=['GET'])
def init_table():
	try:
		RoleModel.create_table()
		return create_json_response("Done!")
	except Exception as error:
		handle_error(error)


@roles.route("", methods=['GET'])
def get_all():
	try:
		req_args = request.args

		properties = {key: value for key, value in req_args.iteritems()
					  if key in RoleModel.__table__.columns}
		properties['is_deleted'] = properties.get('is_deleted', False)

		response = {
			'data': RoleModel.get_all(req_args, **properties),
			'total_count': RoleModel.count(**properties)
		}
		return create_json_response(response)

	except Exception as error:
		handle_error(error)


@roles.route('/<url_safe>', methods=['GET'])
def get(url_safe):
	"""Display a certain RoleModel entity.

    Args:
        url_key (str): URL-safe key of the entity to be displayed.

    Returns:
        JSON: Updated RoleModel.

    """
	try:
		role = RoleModel.get_by_id(url_safe)
		return create_json_response(role)
	except Exception as error:
		handle_error(error)


@roles.route("", methods=['POST'])
def create():
	"""Create RoleModel entity.

    Args:
        JSON: RoleModel

    Returns:
        JSON: Created RoleModel.
        int: HTTP status code.

    """
	try:
		properties = request.get_json()
		return create_json_response(RoleModel.create_role(**properties))
	except Exception as error:
		handle_error(error)


@roles.route('/<url_safe>', methods=['PUT'])
def update(url_safe):
	"""Updates a certain RoleModel entity.

    Args:
        url_key (str): URL-safe key of the entity to be updated.
    
    Returns:
        JSON: Updated list of the RoleModel.
        int: HTTP status code.

    """
	try:
		updates = request.get_json()
		if 'is_deleted' in updates:
			del updates['is_deleted']

		response = create_json_response(RoleModel.update_role(url_safe, **updates))
		return (response, 200 if updates and len(updates) > 0 else 204)
	except Exception as error:
		handle_error(error)


@roles.route('/<url_safe>', methods=['DELETE'])
def delete(url_safe):
	"""Soft deletes a certain RoleModel entity.

    Args:
        url_safe (str): URL-safe key of the entity to be updated.

    Returns:
        JSON: Deleted RoleModel object.

    """
	try:
		return create_json_response(RoleModel.delete_role(url_safe))
	except Exception as error:
		handle_error(error)

@roles.route("/get_by_ids", methods=['POST'])
def get_role_list_by_ids():
	"""Get the role_id, role_name pairings given a list of role_ids.

    Args:
    	JSON: {
    	      	"role_id" : [...]
    	      }
    Returns:
        JSON: Resulting list.
        int: HTTP status code.

    """
	try:
		role_ids = dict(request.get_json())["role_id"]
		response = {
            "roles": RoleModel.get_role_list_by_ids(role_ids)
        }

		return create_json_response(response)
	except Exception as error:
		handle_error(error)
