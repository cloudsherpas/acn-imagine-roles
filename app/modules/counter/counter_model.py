import logging
from app.exceptions import EntityNotFoundException, InvalidFormatException
from google.appengine.ext import ndb
from google.appengine.api.datastore_errors import BadValueError
from core.ndb import BasicModel


class CounterModel(BasicModel):
    module_name = ndb.StringProperty(indexed=True)
    counter_value = ndb.IntegerProperty(indexed=True, default=1)

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, key, item):
        return setattr(self, key, item)

    @classmethod
    def get_counter_role(cls):
        role_counter = CounterModel.query(CounterModel.module_name=="ROLE").get_async().get_result()
        if role_counter is None:
            return CounterModel.create_role_id()
        else:
            CounterModel.update_role_id()
            return role_counter

    @classmethod
    def create_role_id(cls):
        counter_create = CounterModel()
        counter_create.module_name = "ROLE"
        counter_create.put()
        return counter_create

    @classmethod
    def update_role_id(cls):
        role_counter = CounterModel.query(CounterModel.module_name=="ROLE").get_async().get_result()
        role_counter['counter_value'] += 1
        return role_counter.put()

